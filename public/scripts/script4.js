//калькулятор

// текстовое поле с математическим выражением
var display = document.getElementById('display');

//расчет математический выражений
function addChar(input, character) {
    if (input.value == null || input.value == "0")
        input.value = character
    else
        input.value += character
}

function cos(form) { form.display.value = Math.cos(form.display.value); }

function sin(form) { form.display.value = Math.sin(form.display.value); }

function tan(form) { form.display.value = Math.tan(form.display.value); }

function sqrt(form) { form.display.value = Math.sqrt(form.display.value); }

function ln(form) { form.display.value = Math.log(form.display.value); }

function exp(form) { form.display.value = Math.exp(form.display.value); }

function sqrt(form) { form.display.value = Math.sqrt(form.display.value); }

function changeSign(input) {
    if (input.value.substring(0, 1) == "-")
        input.value = input.value.substring(1, input.value.length)
    else
        input.value = "-" + input.value
}

function compute(form) { form.display.value = eval(form.display.value); }

function square(form) { form.display.value = eval(form.display.value) * eval(form.display.value); }

function deleteChar(input) { input.value = input.value.substring(0, input.value.length - 1); }

//неверный ввод
function checkNum(str) {
    for (var i = 0; i < str.length; i++) {
        var ch = str.substring(i, i + 1)
        if (ch < "0" || ch > "9") {
            if (ch != "/" && ch != "*" && ch != "+" && ch != "-" && ch != "."
                && ch != "(" && ch != ")") {
                alert("invalid entry!")
                return false
            }
        }
    }
    return true
}

var myButtonSqrt = document.getElementById('sqrt');
myButtonSqrt.onclick = function () {
    if (checkNum(this.form.display.value)) { sqrt(this.form) };
}

var myButton1 = document.getElementById('but1');
myButton1.onclick = function () {
    display = addChar(this.form.display, '1');
}

var myButton2 = document.getElementById('but2');
myButton2.onclick = function () {
    display = addChar(this.form.display, '2');
}

var myButton3 = document.getElementById('but3');
myButton3.onclick = function () {
    display = addChar(this.form.display, '3');
}

var myButtonMinus = document.getElementById('minus');
myButtonMinus.onclick = function () {
    display = addChar(this.form.display, '-');
}


var myButtonLn = document.getElementById('ln');
myButtonLn.onclick = function () {
    if (checkNum(this.form.display.value)) { ln(this.form) };
}

var myButton4 = document.getElementById('4');
myButton4.onclick = function () {
    display = addChar(this.form.display, '4');
}

var myButton5 = document.getElementById('5');
myButton5.onclick = function () {
    display = addChar(this.form.display, '5');
}

var myButton6 = document.getElementById('6');
myButton6.onclick = function () {
    display = addChar(this.form.display, '6');
}

var myButtonStar = document.getElementById('star');
myButtonStar.onclick = function () {
    display = addChar(this.form.display, '*');
}


var myButtonExp = document.getElementById('exp');
myButtonExp.onclick = function () {
    if (checkNum(this.form.display.value)) { exp(this.form) };
}

var myButton7 = document.getElementById('7');
myButton7.onclick = function () {
    display = addChar(this.form.display, '7');
}

var myButton8 = document.getElementById('8');
myButton8.onclick = function () {
    display = addChar(this.form.display, '8');
}

var myButton9 = document.getElementById('9');
myButton9.onclick = function () {
    display = addChar(this.form.display, '9');
}

var myButtonSlash = document.getElementById('slash');
myButtonSlash.onclick = function () {
    display = addChar(this.form.display, '/');
}

var myButtonSq = document.getElementById('sq');
myButtonSq.onclick = function () {
    if (checkNum(this.form.display.value)) { square(this.form) };
}

var myButtonNul = document.getElementById('nul');
myButtonNul.onclick = function () {
    display = addChar(this.form.display, '0');
}

var myButtonPoint = document.getElementById('point');
myButtonPoint.onclick = function () {
    display = addChar(this.form.display, '.');
}

var myButtonChangeSign = document.getElementById('changeSign');
myButtonChangeSign.onclick = function () {
    display = changeSign(this.form.display);
}

var myButtonPlus = document.getElementById('plus');
myButtonPlus.onclick = function () {
    display = addChar(this.form.display, '+');
}

var myButtonBracket = document.getElementById('bracket');
myButtonBracket.onclick = function () {
    display = addChar(this.form.display, '(');
}

var myButtonCos = document.getElementById('cos');
myButtonCos.onclick = function () {
    if (checkNum(this.form.display.value)) { cos(this.form) };
}

var myButtonSin = document.getElementById('sin');
myButtonSin.onclick = function () {
    if (checkNum(this.form.display.value)) { sin(this.form) };
}

var myButtonTan = document.getElementById('tan');
myButtonTan.onclick = function () {
    if (checkNum(this.form.display.value)) { tan(this.form) };
}

var myButtonBracket2 = document.getElementById('bracket2');
myButtonBracket2.onclick = function () {
    display = addChar(this.form.display, ')');
}

var myButtonClear = document.getElementById('Clear');
myButtonClear.onclick = function () {
    this.form.display.value = 0;
}

var myButtonBackSpace = document.getElementById('BackSpace');
myButtonBackSpace.onclick = function () {
    display = deleteChar(this.form.display);
}

var myButtonEnter = document.getElementById('Enter');
myButtonEnter.onclick = function () {
    if (checkNum(this.form.display.value)) { compute(this.form) };
}
