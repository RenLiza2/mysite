// переключение статей

var myButton1 = document.getElementById('buttonArt1');
myButton1.onclick = function () {
    var elem1 = document.getElementById('article1');
    elem1.hidden = false;
    var elem2 = document.getElementById('article2');
    elem2.hidden = true;
    var elem3 = document.getElementById('article3');
    elem3.hidden = true;
    var elem4 = document.getElementById('article4');
    elem4.hidden = true;
}

var myButton2 = document.getElementById('buttonArt2');
myButton2.onclick = function () {
    var elem1 = document.getElementById('article1');
    elem1.hidden = true;
    var elem2 = document.getElementById('article2');
    elem2.hidden = false;
    var elem3 = document.getElementById('article3');
    elem3.hidden = true;
    var elem4 = document.getElementById('article4');
    elem4.hidden = true;
}

var myButton3 = document.getElementById('buttonArt3');
myButton3.onclick = function () {
    var elem1 = document.getElementById('article1');
    elem1.hidden = true;
    var elem2 = document.getElementById('article2');
    elem2.hidden = true;
    var elem3 = document.getElementById('article3');
    elem3.hidden = false;
    var elem4 = document.getElementById('article4');
    elem4.hidden = true;
}

var myButton4 = document.getElementById('buttonArt4');
myButton4.onclick = function () {
    var elem1 = document.getElementById('article1');
    elem1.hidden = true;
    var elem2 = document.getElementById('article2');
    elem2.hidden = true;
    var elem3 = document.getElementById('article3');
    elem3.hidden = true;
    var elem4 = document.getElementById('article4');
    elem4.hidden = false;
}

