//переходы между элементами на одной странице
document.onclick = function (event) {
   var target = event.target;
   var id = target.getAttribute('data-list');
   if (!id) return;
   var elem = document.getElementById(id);
   elem.hidden = !elem.hidden;
}

//постенькая проверка формы

function validateComments(input) {
    if (input.value.length < 6) {
        input.setCustomValidity("At least 6 characters");
    }
    else {
        input.setCustomValidity("");
    }
}

var signInBut = document.getElementById('SignIn');
signInBut.onclick = function () {
    var LogLogin = document.getElementById('LogLogin');
    LogLogin = validateComments(LogLogin);
    var LogPassword = document.getElementById('LogPassword');
    LogPassword = validateComments(LogPassword);
}

var signIn2But = document.getElementById('SignIn2');
signIn2But.onclick = function () {
    var Name = document.getElementById('name');
    Name = validateComments(Name);
    var RegLogin = document.getElementById('RegLogin');
    RegLogin = validateComments(RegLogin);
    var RegPassword = document.getElementById('RegPassword');
    RegPassword = validateComments(RegPassword);
    var email = document.getElementById('email');
    email = validateComments(email);
}

//секундомер
startday = new Date();
clockStart = startday.getTime();

function initStopwatch() {
    var myTime = new Date();
    var timeNow = myTime.getTime();
    var timeDiff = timeNow - clockStart;
    this.diffSecs = timeDiff / 1000;
    return (this.diffSecs);
}

function getSecs() {
    var mySecs = initStopwatch();
    var mySecs1 = "" + mySecs;
    mySecs1 = mySecs1.substring(0, mySecs1.indexOf(".")) + " secs.";
    document.forms[2].timespent.value = mySecs1
    window.setTimeout('getSecs()', 1000);
}
